Readme.txt
Created 13th March 2009 by Nick Young
www.nickbits.co.uk
*********************************

Information
------------
This is a port of 1024px. This theme was originally designed by Andreas Viklund (andreasviklund.com) as a free website template. He states (http://andreasviklund.com/about/copyright/) "The free website templates (and all included images and related extras) are released as open source web design. It means that you may use the templates for free for any purpose, personal as well as commercial, without any obligations or conditions."

Please do let me know of any bugs or features you would like to see.

Features
--------
This theme has the following features:

* Site name
* Logo
* Slogan
* Mission
* Left and right sidebars - in any configuration

Request
--------
The only thing I ask of you if you use this theme is that you leave the links to my and Andreas sites in the footer intact.  Andreas produced the theme and I have ported it to Drupal, both for free.

Install
-------
Install the theme as you would for any other theme.


FAQ
-----
1. How do I use primary and secondary menus?
A. To use primary and secondary menus you will need to add them in as blocks.

2.  Can I you add feature xyz?
A.  Yes.  However it depends specifically on the feature and timeframe.  I do not earn any money from this and do it in my spare time.  I attempt to accomodate any changes requested, although this does obviously have to fit in with when I can get the time. If you wish to make a donation or pay me to do work, please contact me via nickbits.co.uk/contact.